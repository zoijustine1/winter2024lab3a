public class Application {
	public static void main(String[] args){
		Student firstStudent = new Student();
		firstStudent.name = "Byron";
		firstStudent.age = 32;
		firstStudent.program = "Business Program";
		/*System.out.println(firstStudent.name);
		System.out.println(firstStudent.age);
		System.out.println(firstStudent.program);*/
		Student secondStudent = new Student();
		secondStudent.name = "Zoi Justine";
		secondStudent.age = 10;
		secondStudent.program = "Computer Science";
		/*System.out.println(secondStudent.name);
		System.out.println(secondStudent.age);
		System.out.println(secondStudent.program);*/
		
		//firstStudent.sayProgram();
		//secondStudent.introduceSelf();
		
		Student[] section3 = new Student[3];
		section3[0] = new Student();
		section3[0].name = "Byron Gay";
		
		section3[1] = secondStudent;
		section3[2] = new Student();
		section3[2].name = "FRANK";
		section3[2].program = "Engineer";
		section3[2].age = 19;
		System.out.println(section3[2].program);
		System.out.println(section3[2].name);
		System.out.println(section3[2].age);
	}
}