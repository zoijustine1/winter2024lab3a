public class Student {
	public String name;
	public String program;
	public int age;
	
	public void introduceSelf(){
		System.out.println("Hello my name is " + this.name + " I'm " + this.age + " years old");
		
	}
	
	public void sayProgram(){
		System.out.println("My program is " + this.program);
	}
}